import Foundation

class TaskListPresenter: TaskListPresenterProtocol {
    
    var wireframe: TaskListWireFrameProtocol?
    weak var view: TaskListViewProtocol?
    var interactor: TaskListInputInteractorProtocol?

    func viewDidLoad() {
        self.loadTaskList()
    }

    func loadTaskList() {
        interactor?.getTaskList()
    }
    
}

extension TaskListPresenter: TaskListOutputInteractorProtocol {
    func taskListDidFetch(taskList: [MyTask]) {
        view?.showTasks(with: taskList)
    }
}
