import Foundation
import CoreData
import UIKit

class TaskListInteractor: TaskListInputInteractorProtocol {
    
    weak var presenter: TaskListOutputInteractorProtocol?
    
    func getTaskList() {
        presenter?.taskListDidFetch(taskList: getOldTasks())
    }
    
    func getOldTasks() -> [MyTask] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MyTask")
        request.predicate = NSPredicate(format: "removed == %@ or finished == %@", NSNumber(value: true), NSNumber(value: true))
        request.sortDescriptors = [NSSortDescriptor(key: "createdAt", ascending: false)]
        
        var list: [MyTask] = []
        
        do {
            let results: NSArray = try context.fetch(request) as NSArray
            for result in results {
                let task = result as! MyTask
                list.append(task)
            }
        } catch {
            print("Fetch failed")
        }
        
        return list
    }
}
