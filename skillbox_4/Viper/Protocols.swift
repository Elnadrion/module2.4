import Foundation
import UIKit

protocol TaskListViewProtocol: AnyObject {
    // PRESENTER -> VIEW
    func showTasks(with tasks: [MyTask])
}

protocol TaskListPresenterProtocol: AnyObject {
    //View -> Presenter
    var interactor: TaskListInputInteractorProtocol? {get set}
    var view: TaskListViewProtocol? {get set}
    var wireframe: TaskListWireFrameProtocol? {get set}

    func viewDidLoad()
//    func showTaskSelection(with task: MyTask, from view: UITableViewController)
}

protocol TaskListInputInteractorProtocol: AnyObject {
    var presenter: TaskListOutputInteractorProtocol? {get set}
    //Presenter -> Interactor
    func getTaskList()
}

protocol TaskListOutputInteractorProtocol: AnyObject {
    //Interactor -> Presenter
    func taskListDidFetch(taskList: [MyTask])
}

protocol TaskListWireFrameProtocol: AnyObject {
    //Presenter -> Wireframe
    static func createTaskListModule(taskListRef: OldTaskViewControllerTableViewController)
}
