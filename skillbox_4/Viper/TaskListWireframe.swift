import Foundation

import UIKit

class TaksListWireframe: TaskListWireFrameProtocol {
    class func createTaskListModule(taskListRef: OldTaskViewControllerTableViewController) {
       let presenter: TaskListPresenterProtocol & TaskListOutputInteractorProtocol = TaskListPresenter()
        
        taskListRef.presenter = presenter
        taskListRef.presenter?.wireframe = TaksListWireframe()
        taskListRef.presenter?.view = taskListRef
        taskListRef.presenter?.interactor = TaskListInteractor()
        taskListRef.presenter?.interactor?.presenter = presenter
    }
}
