import UIKit
import CoreData

class TaskListViewController: UITableViewController {
    
    var taskList = [MyTask]()
    let taskViewModel = TaskViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        taskList = taskViewModel.getActiveTasks()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let taskCell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! TaskTableViewCell
        
        taskCell.fillWithData(task: taskList[indexPath.row])

        return taskCell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") {  (contextualAction, view, boolValue) in
            let task = self.taskList[indexPath.row]
            self.taskList.remove(at: indexPath.row)
            
            self.taskViewModel.remoteTask(task: task)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        let finishAction = UIContextualAction(style: .normal, title: "Finish") {  (contextualAction, view, boolValue) in
            let task = self.taskList[indexPath.row]
            self.taskList.remove(at: indexPath.row)
            
            self.taskViewModel.finishTask(task: task)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        return UISwipeActionsConfiguration(actions: [deleteAction, finishAction])
    }

}
