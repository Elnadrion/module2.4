import UIKit

class OldTaskViewControllerTableViewController: UITableViewController {
    
    var oldTaskList = [MyTask]()
    var presenter: TaskListPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        TaksListWireframe.createTaskListModule(taskListRef: self)
        presenter?.viewDidLoad()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let taskCell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! TaskTableViewCell
        
        taskCell.fillWithData(task: oldTaskList[indexPath.row])
        
        return taskCell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return oldTaskList.count
    }
}

extension OldTaskViewControllerTableViewController: TaskListViewProtocol {
    func showTasks(with: [MyTask]) {
        self.oldTaskList = with
        self.tableView.reloadData()
    }
}
