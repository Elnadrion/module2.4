import UIKit
import CoreData

class AddTaskViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var deadLineTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var deadlineDate: Date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        descriptionTextView.layer.borderWidth = 0.5
        descriptionTextView.layer.borderColor = borderColor.cgColor
        descriptionTextView.layer.cornerRadius = 5
        descriptionTextView.textContainer.lineFragmentPadding = 5
        
        self.deadLineTextField.datePicker(target: self,
                                  doneAction: #selector(doneAction),
                                  cancelAction: #selector(cancelAction))
    }
    
    @objc
    func cancelAction() {
        self.deadLineTextField.resignFirstResponder()
    }

    @objc
    func doneAction() {
        if let datePickerView = self.deadLineTextField.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy H:mm"
            let dateString = dateFormatter.string(from: datePickerView.date)
            
            self.deadLineTextField.text = dateString
            self.deadLineTextField.resignFirstResponder()
            self.deadlineDate = datePickerView.date
        }
    }
    
    @IBAction func createTask(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "MyTask", in: context)
        let task = MyTask(entity: entity!, insertInto: context)
        
        task.title = titleTextField.text!
        task.deadline = deadlineDate
        task.taskDescription = descriptionTextView.text!
        task.createdAt = Date()
  
        do {
            try context.save()
            performSegue(withIdentifier: "toTaskList", sender: nil)
        } catch {
            print("error")
        }
       
        
    }
}
