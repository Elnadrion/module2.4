import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    @IBOutlet weak var deadline: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var taskDescription: UILabel!
    
    let dateFormatter = DateFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func fillWithData(task: MyTask) {
        dateFormatter.dateFormat = "dd.MM.yyyy H:mm"
        
        title.text = task.title
        createdAt.text = "Создана: \(dateFormatter.string(from: task.createdAt!))"
        taskDescription.text = task.taskDescription
        
        if task.deadline != nil {
            deadline.text = "Дедлайн: \(dateFormatter.string(from: task.deadline!))"
        }
        
        if task.removed {
            status.text = "Удалена"
        } else if (task.finished) {
            status.text = "Завершена"
        } else {
            status.text = "Создана"
        }
    }}
