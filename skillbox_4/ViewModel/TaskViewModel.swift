import Foundation
import CoreData
import UIKit

class TaskViewModel {
    
    func getActiveTasks() -> [MyTask] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MyTask")
        request.predicate = NSPredicate(format: "removed != %@ and finished != %@", NSNumber(value: true), NSNumber(value: true))
        request.sortDescriptors = [NSSortDescriptor(key: "createdAt", ascending: false)]
        
        var list: [MyTask] = []
        
        do {
            let results: NSArray = try context.fetch(request) as NSArray
            for result in results {
                let task = result as! MyTask
                list.append(task)
            }
        } catch {
            print("Fetch failed")
        }
        
        return list
    }
    
    func remoteAllTasks() -> Void {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MyTask")
        let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try context.execute(batchDeleteRequest)

        } catch {
            print("Remove failed")
        }
    }
    
    private func updateTask(task: MyTask) -> Void {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
        
        do {
            try context.save()

        } catch {
            print("Update failed")
        }
    }
    
    func remoteTask(task: MyTask) -> Void {
        task.removed = true
        updateTask(task: task)
    }
    
    func finishTask(task: MyTask) -> Void {
        task.finished = true
        updateTask(task: task)
    }
}
